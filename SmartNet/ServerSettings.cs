﻿using SerializeField = UnityEngine.SerializeField;
using CreateAssetMenu = UnityEngine.CreateAssetMenuAttribute;
using ScriptableObject = UnityEngine.ScriptableObject;
using System;
using System.Collections.Generic;

namespace SmartNet
{
    [Serializable]
    [CreateAssetMenu]
    public class ServerSettings : ScriptableObject
    {
        [SerializeField] private ConnectionConfig defaultConfig = new ConnectionConfig();
        [SerializeField] private int maxConnections = 16;
        [SerializeField] private List<ConnectionConfig> specialConnectionConfigs = new List<ConnectionConfig>();
        [SerializeField] private ushort receivedMessagePoolSize = 1024;
        [SerializeField] private ushort sentMessagePoolSize = 1024;
        [SerializeField] private float messagePoolSizeGrowthFactor = 0.75f;

        public ConnectionConfig DefaultConfig => defaultConfig;
        public int MaxConnections => maxConnections;
        public int SpecialConnectionConfigsCount => specialConnectionConfigs.Count;
        public List<ConnectionConfig> SpecialConnectionConfigs => specialConnectionConfigs;

        public ushort ReceivedMessagePoolSize
        {
            get => receivedMessagePoolSize;
            set => receivedMessagePoolSize = value;
        }

        public ushort SentMessagePoolSize
        {
            get => sentMessagePoolSize;
            set => sentMessagePoolSize = value;
        }

        public float MessagePoolSizeGrowthFactor
        {
            get => messagePoolSizeGrowthFactor;
            set => messagePoolSizeGrowthFactor = UnityEngine.Mathf.Clamp01(value);
        }

        public ServerSettings Initialize(ConnectionConfig config, int newMaxConnections)
        {
            if (config == null)
            {
                throw new NullReferenceException($"{nameof(config)} is not defined");
            }

            if (maxConnections <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(maxConnections), "Number of connections must be greater than 0");
            }

            if (maxConnections >= ushort.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(maxConnections), $"Number of connections must be less than {ushort.MaxValue}");
            }

            ConnectionConfig.Validate(config);

            defaultConfig = new ConnectionConfig(config);
            maxConnections = newMaxConnections;

            return this;
        }

        public ConnectionConfig GetSpecialConnectionConfig(int connectionId)
        {
            if (connectionId > specialConnectionConfigs.Count || connectionId == 0)
            {
                throw new ArgumentException($"{nameof(connectionId)} index is out of valid range");
            }

            return specialConnectionConfigs[connectionId - 1];
        }

        public int AddSpecialConnectionConfig(ConnectionConfig config)
        {
            specialConnectionConfigs.Add(new ConnectionConfig(config));
            return specialConnectionConfigs.Count;
        }

        internal UnityEngine.Networking.HostTopology ToHostTopology()
        {
            var topology = new UnityEngine.Networking.HostTopology(defaultConfig.ToConnectionConfig(), maxConnections)
            {
                MessagePoolSizeGrowthFactor = messagePoolSizeGrowthFactor,
                ReceivedMessagePoolSize = receivedMessagePoolSize,
                SentMessagePoolSize = sentMessagePoolSize
            };

            for (var i = 0; i < specialConnectionConfigs.Count; ++i)
            {
                topology.AddSpecialConnectionConfig(specialConnectionConfigs[i].ToConnectionConfig());
            }

            return topology;
        }

        private ServerSettings()
        {
        }
    }
}
﻿using System.Collections.Generic;
using MonoBehaviour = UnityEngine.MonoBehaviour;
using Image = UnityEngine.UI.Image;
using Vector2 = UnityEngine.Vector2;

namespace SmartNet.Profiler
{
    public class BarGroup : MonoBehaviour
    {
        private readonly List<BarGroupInfo> barInfo = new List<BarGroupInfo>();

        public float Height { get; private set; }

        public void Add(Statistics.NetworkOperationEntryDetails entryDetails, Image image)
        {
            var info = BarGroupInfo.Get();
            info.Initialize(entryDetails, image);
            barInfo.Add(info);

            image.rectTransform.anchoredPosition = new Vector2(0f, Height);
            Height += image.rectTransform.sizeDelta.y;
        }

        public void Recycle(PooledStack<Image> pool)
        {
            for (var i = 0; i < barInfo.Count; ++i)
            {
                pool.Push(barInfo[i].Image);
                barInfo[i].Image.gameObject.SetActive(false);
                BarGroupInfo.Return(barInfo[i]);
            }

            barInfo.Clear();

            gameObject.SetActive(false);
            Height = 0f;
        }
    }

    public class BarGroupInfo
    {
        private static readonly PooledStack<BarGroupInfo> Pool = new PooledStack<BarGroupInfo>(() => new BarGroupInfo(null, null));

        public Statistics.NetworkOperationEntryDetails EntryDetails;
        public Image Image;

        public BarGroupInfo(Statistics.NetworkOperationEntryDetails entryDetails, Image image)
        {
            Initialize(entryDetails, image);
        }

        public void Initialize(Statistics.NetworkOperationEntryDetails entryDetails, Image image)
        {
            EntryDetails = entryDetails;
            Image = image;
        }

        public static BarGroupInfo Get()
        {
            return Pool.Pop();
        }

        public static void Return(BarGroupInfo instance)
        {
            Pool.Push(instance);
        }
    }
}
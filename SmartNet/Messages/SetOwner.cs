﻿namespace SmartNet.Messages
{
    public class SetOwner : INetMessage
    {
        public NetworkId NetworkId;

        public void OnSerialize(Writer writer)
        {
            writer.Write(NetworkId);
        }

        public void OnDeserialize(Reader reader)
        {
            NetworkId = reader.ReadNetworkId();
        }

        public static void HandleMessage(GenericMessageInfo<SetOwner> info)
        {
            var id = info.Message.NetworkId;
            var identity = NetworkScene.KnownIdentities.Get(id);
            identity.SetOwnerInternal(info.Connection);

            if(!identity.IsServer)
            {
                identity.OnStopClient();
            }
            
            identity.OnStartOwner();
        }
    }
}
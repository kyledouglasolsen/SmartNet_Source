﻿using System.Collections.Generic;

namespace SmartNet.Messages
{
    public class WorldDestroy : INetMessage
    {
        public NetworkId NetworkId;

        public void OnSerialize(Writer writer)
        {
            writer.Write(NetworkId);
        }

        public void OnDeserialize(Reader reader)
        {
            NetworkId = reader.ReadNetworkId();
        }

        public static void HandleMessage(GenericMessageInfo<WorldDestroy> info)
        {
            DestroyIds.Push(info.Message.NetworkId);
        }

        private static readonly Stack<NetworkId> DestroyIds = new Stack<NetworkId>();

        static WorldDestroy()
        {
            Updater.PostUpdateEvent += () =>
            {
                while (DestroyIds.Count > 0)
                {
                    var networkId = DestroyIds.Pop();
                    var identity = NetworkScene.KnownIdentities.Get(networkId);

                    if (identity != null)
                    {
                        UnityEngine.Object.Destroy(identity.gameObject);
                    }
                }
            };
        }
    }
}
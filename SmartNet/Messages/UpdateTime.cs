﻿namespace SmartNet.Messages
{
    public class UpdateTime : INetMessage
    {
        public int Time;

        public UpdateTime()
        {
            Time = NetworkTime.Milliseconds;
        }

        public void OnSerialize(Writer writer)
        {
            writer.WriteCompressed(Time);
        }

        public void OnDeserialize(Reader reader)
        {
            Time = reader.ReadIntCompressed();
        }
    }
}
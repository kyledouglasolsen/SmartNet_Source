﻿namespace SmartNet.Messages
{
    public struct UpdateFirstTime : INetMessage
    {
        public int StartTime, Time;

        public void OnSerialize(Writer writer)
        {
            writer.WriteCompressed(NetworkTime.StartMilliseconds);
            writer.WriteCompressed(NetworkTime.Milliseconds);
        }

        public void OnDeserialize(Reader reader)
        {
            StartTime = reader.ReadIntCompressed();
            Time = reader.ReadIntCompressed();
        }
    }
}
﻿namespace SmartNet.Messages
{
    public class WorldSpawn : AssetSpawn
    {
        public static void HandleMessage(MessageInfo info)
        {
            AssetSpawn.HandleMessage(null, info.ReadMessage<WorldSpawn>(), info.Reader);
        }
    }
}
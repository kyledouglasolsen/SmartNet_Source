﻿using UnityEngine;

namespace SmartNet.Messages
{
    public abstract class AssetSpawn : INetMessage
    {
        private SmartNetIdentity writeIdentity;

        public uint AssetId;
        public NetworkId NetworkId;
        public Vector3 Position;
        public Quaternion Rotation;

        protected AssetSpawn()
        {
        }

        protected AssetSpawn(SmartNetIdentity identity)
        {
            Initialize(identity);
        }

        public void Initialize(SmartNetIdentity identity)
        {
            writeIdentity = identity;
            AssetId = identity.AssetId;
            NetworkId = identity.NetworkId;
            Position = identity.transform.position;
            Rotation = identity.transform.rotation;
        }

        public void OnSerialize(Writer writer)
        {
            writer.WriteCompressed(AssetId);
            writer.Write(NetworkId);
            writer.Write(Position);
            writer.WriteCompressed(Rotation);
            writeIdentity.OnSerializeSpawnData(writer);
        }

        public void OnDeserialize(Reader reader)
        {
            AssetId = reader.ReadUIntCompressed();
            NetworkId = reader.ReadNetworkId();
            Position = reader.ReadVector3();
            Rotation = reader.ReadQuaternionCompressed();
        }

        public static void HandleMessage(Connection owner, AssetSpawn message, Reader reader)
        {
            var prefabObject = IdentityLibrary.Get(message.AssetId);
            var instance = Object.Instantiate(prefabObject, message.Position, message.Rotation);
            NetworkScene.Spawn(instance, owner, message.NetworkId, reader);
        }
    }
}
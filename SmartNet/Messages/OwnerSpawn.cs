﻿namespace SmartNet.Messages
{
    public class OwnerSpawn : AssetSpawn
    {
        public static void HandleMessage(MessageInfo info)
        {
            AssetSpawn.HandleMessage(info.Connection, info.ReadMessage<OwnerSpawn>(), info.Reader);
        }
    }
}
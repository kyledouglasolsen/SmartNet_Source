﻿using Vector3 = UnityEngine.Vector3;
using Quaternion = UnityEngine.Quaternion;

namespace SmartNet.Messages
{
    public class AddPlayer : INetMessage
    {
        public void OnSerialize(Writer writer)
        {
        }

        public void OnDeserialize(Reader reader)
        {
        }

        public static void HandleMessage(GenericMessageInfo<AddPlayer> info)
        {
            // ToDo add hook for setting position/rotation for player spawn in callback
            NetworkScene.InstantiatePlayer(info.Connection, Vector3.zero, Quaternion.identity);
        }
    }
}
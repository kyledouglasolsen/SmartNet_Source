﻿using System;
using SmartNet.Profiler;
using Debug = UnityEngine.Debug;

namespace SmartNet.Messages
{
    public class IdentityMessage : INetMessage
    {
        public NetworkId NetworkId;
        public uint MessageType;
        public INetMessage InnerMessage;

        public void OnSerialize(Writer writer)
        {
            writer.Write(NetworkId);
            writer.WriteCompressed(MessageType);
            InnerMessage.OnSerialize(writer);
        }

        public void OnDeserialize(Reader reader)
        {
            NetworkId = reader.ReadNetworkId();
            MessageType = reader.ReadUIntCompressed();
        }

        public void Initialize(NetworkId networkId, uint messageType, INetMessage message)
        {
            NetworkId = networkId;
            MessageType = messageType;
            InnerMessage = message;
        }

        public static void HandleMessage(MessageInfo info)
        {
            var message = info.ReadMessage<IdentityMessage>();
            var identity = NetworkScene.KnownIdentities.Get(message.NetworkId);

            if (identity == null)
            {
                Debug.LogWarning($"Received IdentityMessage<{MessageTypes.GetTypeName(message.MessageType)}> for unknown NetworkId {message.NetworkId}");
                return;
            }

            var startPosition = info.Reader.Position;

            try
            {
                identity.InvokeHandler(message.MessageType, info.Reader);
            }
            catch (Exception e)
            {
                Debug.LogError($"Exception thrown while invoking identity handler {MessageTypes.GetTypeName(message.MessageType)} on SmartNetIdentity with NetId {message.NetworkId}.\n{e}");
            }
            finally
            {
                var readSize = info.Reader.Position - startPosition;
                Statistics.Add<IdentityMessage>(NetworkDirection.Inbound, MessageTypes.GetTypeName(message.MessageType), readSize);
            }
        }
    }
}
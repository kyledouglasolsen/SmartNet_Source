﻿using Debug = UnityEngine.Debug;
using Time = UnityEngine.Time;
using System;
using System.Collections.Generic;
using SmartNet.Messages;

namespace SmartNet
{
    internal class ChannelBuffer : IDisposable
    {
        private readonly Connection connection;
        private ChannelPacket currentPacket;

        private float lastFlushTime;

        private byte channelId;
        private int maxPacketSize;
        private bool isReliable, allowFragmentation, isBroken, readingFragment;
        private int maxPendingPacketCount;
        private static Writer fragmentWriter = new Writer();

        private const int MaxFreePacketCount = 512; //  this is for all connections. maybe make this configurable
        private const int MaxPendingPacketCount = 16; // this is per connection. each is around 1400 bytes (MTU)

        internal Buffer FragmentBuffer;
        private Queue<ChannelPacket> pendingPackets;
        private static Stack<ChannelPacket> freePackets;
        private static byte[] fragmentBytes;

        public float MaxDelay = 0.01f;

        private const int PacketHeaderReserveSize = 100;
        private const int FragmentHeaderSize = 32;

        public ChannelBuffer(Connection connection, int bufferSize, byte cid, bool isReliable, bool isSequenced)
        {
            this.connection = connection;
            maxPacketSize = bufferSize - PacketHeaderReserveSize;
            currentPacket = new ChannelPacket(maxPacketSize, isReliable);

            channelId = cid;
            maxPendingPacketCount = MaxPendingPacketCount;
            this.isReliable = isReliable;
            allowFragmentation = (isReliable && isSequenced);

            if (isReliable)
            {
                pendingPackets = new Queue<ChannelPacket>();

                if (freePackets == null)
                {
                    freePackets = new Stack<ChannelPacket>();
                }
            }

            if (allowFragmentation)
            {
                FragmentBuffer = new Buffer();
                fragmentBytes = new byte[maxPacketSize - FragmentHeaderSize];
            }
        }

        public void CheckInternalBuffer()
        {
            if (Time.realtimeSinceStartup - lastFlushTime < MaxDelay || currentPacket.IsEmpty())
            {
                return;
            }

            SendInternalBuffer();
            lastFlushTime = Time.realtimeSinceStartup;
        }

        public bool SendInternalBuffer()
        {
            if (!isReliable || pendingPackets.Count <= 0)
            {
                return currentPacket.SendToTransport(connection, channelId);
            }

            while (pendingPackets.Count > 0)
            {
                var packet = pendingPackets.Dequeue();

                if (!packet.SendToTransport(connection, channelId))
                {
                    pendingPackets.Enqueue(packet);
                    break;
                }

                FreePacket(packet);

                if (isBroken && pendingPackets.Count < (maxPendingPacketCount / 2))
                {
                    Debug.LogWarning("ChannelBuffer recovered from overflow but data was lost.");
                    isBroken = false;
                }
            }

            return true;
        }

        public bool Send(Writer writer)
        {
            return Send(writer.AsArray(), (int)writer.Position);
        }

        public bool Send(byte[] bytes, int bytesToSend)
        {
            if (bytesToSend <= 0)
            {
                Debug.LogError("ChannelBuffer can not send zero bytes.");
                return false;
            }

            if (bytesToSend > maxPacketSize)
            {
                if (allowFragmentation)
                {
                    return SendFragmentBytes(bytes, bytesToSend);
                }

                Debug.LogError($"Failed to send big message of {bytesToSend} bytes. The maximum is {maxPacketSize} bytes on this channel.");
                return false;
            }

            if (!currentPacket.HasSpace(bytesToSend))
            {
                if (isReliable)
                {
                    if (pendingPackets.Count == 0)
                    {
                        if (!currentPacket.SendToTransport(connection, channelId))
                        {
                            QueuePacket();
                        }

                        currentPacket.Write(bytes, bytesToSend);
                        return true;
                    }

                    if (pendingPackets.Count >= maxPendingPacketCount)
                    {
                        if (!isBroken)
                        {
                            Debug.LogError($"ChannelBuffer buffer limit of {pendingPackets.Count} packets reached.");
                        }

                        isBroken = true;
                        return false;
                    }

                    QueuePacket();
                    currentPacket.Write(bytes, bytesToSend);
                    return true;
                }

                if (!currentPacket.SendToTransport(connection, channelId))
                {
                    Debug.Log($"ChannelBuffer SendBytes no space on unreliable channel {channelId}");
                    return false;
                }

                currentPacket.Write(bytes, bytesToSend);
                return true;
            }

            currentPacket.Write(bytes, bytesToSend);

            if (MaxDelay <= 0f)
            {
                return SendInternalBuffer();
            }

            return true;
        }

        internal bool HandleFragment(Reader reader)
        {
            int state = reader.ReadByte();

            if (state == 0)
            {
                if (!readingFragment)
                {
                    FragmentBuffer.SeekZero();
                    readingFragment = true;
                }

                var data = reader.ReadBytesAndSize();
                FragmentBuffer.WriteBytes(data, (uint)data.Length);
                return false;
            }

            readingFragment = false;
            return true;
        }

        private bool SendFragmentBytes(byte[] bytes, int bytesToSend)
        {
            var pos = 0;

            while (bytesToSend > 0)
            {
                var diff = Math.Min(bytesToSend, maxPacketSize - FragmentHeaderSize);
                Array.Copy(bytes, pos, fragmentBytes, 0, diff);

                fragmentWriter.SeekZero();
                fragmentWriter.WriteMessageType<FragmentedMessage>();
                fragmentWriter.Write((byte)0);
                fragmentWriter.WriteBytesAndSize(fragmentBytes, (uint)diff);
                Send(fragmentWriter);

                pos += diff;
                bytesToSend -= diff;
            }

            fragmentWriter.SeekZero();
            fragmentWriter.WriteMessageType<FragmentedMessage>();
            fragmentWriter.Write((byte)1);
            Send(fragmentWriter);

            return true;
        }

        private void QueuePacket()
        {
            pendingPackets.Enqueue(currentPacket);
            currentPacket = AllocPacket();
        }

        private ChannelPacket AllocPacket()
        {
            if (freePackets.Count == 0)
            {
                return new ChannelPacket(maxPacketSize, isReliable);
            }

            var packet = freePackets.Pop();
            packet.Reset();

            return packet;
        }

        private static void FreePacket(ChannelPacket packet)
        {
            if (freePackets.Count >= MaxFreePacketCount)
            {
                return;
            }

            freePackets.Push(packet);
        }

        private bool disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                if (pendingPackets != null)
                {
                    while (pendingPackets.Count > 0)
                    {
                        var packet = pendingPackets.Dequeue();

                        if (freePackets.Count < MaxFreePacketCount)
                        {
                            freePackets.Push(packet);
                        }
                    }

                    pendingPackets.Clear();
                }
            }

            disposed = true;
        }
    }
}
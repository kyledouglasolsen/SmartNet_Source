﻿using System;
using System.Collections.Generic;
using SerializeField = UnityEngine.SerializeField;
using HideInInspector = UnityEngine.HideInInspector;
using MonoBehaviour = UnityEngine.MonoBehaviour;
using Debug = UnityEngine.Debug;
using SmartNet.Messages;
using SmartNet.Profiler;

namespace SmartNet
{
    public class SmartNetIdentity : MonoBehaviour
    {
        private static readonly PooledStack<HashSet<SmartNetIdentity>> OwnerObserverPool = new PooledStack<HashSet<SmartNetIdentity>>(() => new HashSet<SmartNetIdentity>());

        private static uint nextNetworkId = 1u;
        private static readonly uint IdentityMessageId = MessageTypes.GetTypeId<IdentityMessage>();
        private static readonly SetOwner SetOwnerMessage = new SetOwner();
        private static readonly RemoveOwner RemoveOwnerMessage = new RemoveOwner();
        private static readonly IdentityMessage IdentityMessage = new IdentityMessage();

        [SerializeField, HideInInspector] private uint assetId;
        [SerializeField, HideInInspector] private NetworkId networkId;
        [HideInInspector] private Connection owner;
        [HideInInspector] private bool isServer, isOwner, isClient, isPlayer;
        private readonly IdentityHandlers handlers = new IdentityHandlers();
        private INetEvent[] netEvents;
        private INetVisibility visibility;
        private readonly HashSet<SmartNetIdentity> visible = new HashSet<SmartNetIdentity>();
        private readonly Dictionary<Connection, HashSet<SmartNetIdentity>> observers = new Dictionary<Connection, HashSet<SmartNetIdentity>>();

        public uint AssetId => assetId;
        public NetworkId NetworkId => networkId;
        public Connection Owner => owner;
        public bool IsServer => isServer;
        public bool IsOwner => isOwner;
        public bool IsClient => isClient;
        public bool IsPlayer => isPlayer;
        public IEnumerable<Connection> Observers => observers.Keys;
        public IEnumerable<SmartNetIdentity> Visible => visible;

        public void SetNetworkId(NetworkId newNetId)
        {
            networkId = newNetId;
        }

        public void SetIsServer(bool newIsServer)
        {
            isServer = newIsServer;
            isClient = !isServer && !isOwner;
        }

        public void SetIsPlayer(bool newIsPlayer)
        {
            isPlayer = newIsPlayer;
        }

        public void ReplaceOwner(Connection connection)
        {
            if (!IsServer)
            {
                return;
            }

            if (owner == connection)
            {
                return;
            }

            if (owner != null)
            {
                RemoveOwner();
            }

            SetOwner(connection);
        }

        public void SetOwner(Connection connection)
        {
            if (!IsServer)
            {
                return;
            }

            if (owner != null)
            {
                Debug.LogError($"Failed setting owner! Identity is already owned by {Owner}.");
                return;
            }

            SetOwnerInternal(connection);

            SetOwnerMessage.NetworkId = NetworkId;
            connection.Send(SetOwnerMessage, 1);
        }

        public void RemoveOwner()
        {
            if (!IsServer)
            {
                return;
            }

            if (owner == null)
            {
                Debug.LogError("Failed removing owner! Identity isn't currently owned by anyone.");
                return;
            }

            var connection = owner;
            owner = null;
            isOwner = false;
            isClient = !isServer && !isOwner;

            for (var i = 0; i < netEvents.Length; ++i)
            {
                (netEvents[i] as IServerOwnerChanged)?.OnServerOwnerRemoved(connection);
            }

            RemoveOwnerMessage.NetworkId = NetworkId;
            connection.Send(RemoveOwnerMessage, 1);
        }

        public void OnPreStart()
        {
            if (IsServer)
            {
                if (visibility == null)
                {
                    visibility = GetComponent<INetVisibility>();
                }

                if (visibility == null)
                {
                    visibility = gameObject.AddComponent<DefaultNetVisibility>();
                }
            }

            netEvents = GetComponents<INetEvent>();

            for (var i = 0; i < netEvents.Length; ++i)
            {
                (netEvents[i] as IPreStart)?.OnPreStart();
            }
        }

        public void OnPreStop()
        {
            for (var i = 0; i < netEvents.Length; ++i)
            {
                (netEvents[i] as IPreStop)?.OnPreStop();
            }
        }

        public void OnStartServer()
        {
            for (var i = 0; i < netEvents.Length; ++i)
            {
                (netEvents[i] as IStartServer)?.OnStartServer();
            }
        }

        public void OnStopServer()
        {
            for (var i = 0; i < netEvents.Length; ++i)
            {
                (netEvents[i] as IStopServer)?.OnStopServer();
            }
        }

        public void OnStartOwner()
        {
            for (var i = 0; i < netEvents.Length; ++i)
            {
                (netEvents[i] as IStartOwner)?.OnStartOwner();
            }
        }

        public void OnStopOwner()
        {
            for (var i = 0; i < netEvents.Length; ++i)
            {
                (netEvents[i] as IStopOwner)?.OnStopOwner();
            }
        }

        public void OnStartClient()
        {
            for (var i = 0; i < netEvents.Length; ++i)
            {
                (netEvents[i] as IStartClient)?.OnStartClient();
            }
        }

        public void OnStopClient()
        {
            for (var i = 0; i < netEvents.Length; ++i)
            {
                (netEvents[i] as IStopClient)?.OnStopClient();
            }
        }

        public void RegisterHandler<T>(GenericMessageDelegate<T> handler) where T : INetMessage, new()
        {
            var messageType = MessageTypes.GetTypeId<T>();

            RegisterHandler(messageType, new T(), message =>
            {
                var generic = GenericMessageInfo<T>.Pop(messageType, Owner, (T)message);
                handler(generic);
                GenericMessageInfo<T>.Push(generic);
            });
        }

        public void RegisterHandler<T>(GenericMessageDelegate<T> handler, T message) where T : INetMessage, new()
        {
            var messageType = MessageTypes.GetTypeId<T>();
            var generic = new GenericMessageInfo<T>(messageType, null, message);

            RegisterHandler(messageType, message, info =>
            {
                generic.Connection = Owner;
                handler(generic);
            });
        }

        public void UnregisterHandler<T>() where T : INetMessage
        {
            UnregisterHandler(MessageTypes.GetTypeId(typeof(T)));
        }

        public void RegisterHandler(uint messageType, INetMessage cachedMessage, IdentityMessageDelegate handler)
        {
            handlers.Register(messageType, cachedMessage, handler);
        }

        public void UnregisterHandler(uint messageType)
        {
            handlers.Unregister(messageType);
        }

        public void InvokeHandler(uint messageType, Reader reader)
        {
            INetMessage message;

            try
            {
                message = GetCachedMessage(messageType);
                message.OnDeserialize(reader);
            }
            catch (Exception e)
            {
                Debug.LogError($"Failed to deserialize {MessageTypes.GetTypeName(messageType)} message on SmartNetIdentity. {e}");
                return;
            }

            var callback = handlers.Get(messageType);

            if (callback == null)
            {
                Debug.LogError($"No callback registered for message {MessageTypes.GetTypeName(messageType)} on SmartNetIdentity.");
                return;
            }

            callback.Handler(message);
        }

        private static WorldSpawn worldSpawn = new WorldSpawn();
        private static uint worldSpawnMessageId = MessageTypes.GetTypeId<WorldSpawn>();
        private static OwnerSpawn ownerSpawn = new OwnerSpawn();
        private static uint ownerSpawnMessageId = MessageTypes.GetTypeId<OwnerSpawn>();

        public bool AddVisible(SmartNetIdentity identity)
        {
            if (identity == null)
            {
                return false;
            }

            TryAddObserver(identity);

            var added = visible.Add(identity);

            if (added)
            {
                if (Owner != null && IsPlayer)
                {
                    if (identity.Owner == Owner)
                    {
                        ownerSpawn.Initialize(identity);
                        Owner.Send(ownerSpawnMessageId, ownerSpawn, 0);
                    }
                    else
                    {
                        worldSpawn.Initialize(identity);
                        Owner.Send(worldSpawnMessageId, worldSpawn, 0);
                    }
                }

                for (var i = 0; i < netEvents.Length; ++i)
                {
                    (netEvents[i] as IServerVisibilityChanged)?.OnAddVisible(identity);
                }
            }

            return added;
        }

        private static WorldDestroy worldDestroy = new WorldDestroy();
        private static uint worldDestroyId = MessageTypes.GetTypeId<WorldDestroy>();

        public bool RemoveVisible(SmartNetIdentity identity)
        {
            if (identity == null)
            {
                return false;
            }

            TryRemoveObserver(identity);

            var removed = visible.Remove(identity);

            if (removed)
            {
                if (Owner != null && IsPlayer)
                {
                    worldDestroy.NetworkId = identity.NetworkId;
                    Owner.Send(worldDestroyId, worldDestroy, 0);
                }

                for (var i = 0; i < netEvents.Length; ++i)
                {
                    (netEvents[i] as IServerVisibilityChanged)?.OnRemoveVisible(identity);
                }
            }

            return removed;
        }

        public bool HasVisible(SmartNetIdentity identity)
        {
            return visible.Contains(identity);
        }

        public bool Send<T>(T message, int channelId) where T : INetMessage
        {
            return Send(MessageTypes.GetTypeId<T>(), message, channelId);
        }

        public bool Send(uint messageType, INetMessage message, int channelId)
        {
            IdentityMessage.Initialize(NetworkId, messageType, message);

            var byteCount = 0u;
            var sent = Owner != null && Owner.Send(IdentityMessageId, IdentityMessage, channelId, out byteCount);

            if (sent)
            {
                Statistics.Add<IdentityMessage>(NetworkDirection.Outbound, MessageTypes.GetTypeName(messageType), byteCount);
            }

            return sent;
        }

        public bool SendToAll<T>(T message, int channelId) where T : INetMessage
        {
            return SendToAll(MessageTypes.GetTypeId<T>(), message, channelId);
        }

        public bool SendToAll(uint messageType, INetMessage message, int channelId)
        {
            IdentityMessage.Initialize(NetworkId, messageType, message);
            return NetworkServer.SendToAll(IdentityMessageId, IdentityMessage, channelId);
        }

        public bool SendToObservers<T>(T message, int channelId) where T : INetMessage
        {
            return SendToObservers(MessageTypes.GetTypeId<T>(), message, channelId);
        }

        public bool SendToObservers(uint messageType, INetMessage message, int channelId)
        {
            IdentityMessage.Initialize(NetworkId, messageType, message);
            return NetworkServer.SendToConnections(Observers, IdentityMessageId, IdentityMessage, channelId);
        }

        public void OnSerializeSpawnData(Writer writer)
        {
            for (var i = 0; i < netEvents.Length; ++i)
            {
                (netEvents[i] as IHaveSpawnData)?.OnSerializeSpawnData(writer);
            }
        }

        public void OnDeserializeSpawnData(Reader reader)
        {
            for (var i = 0; i < netEvents.Length; ++i)
            {
                (netEvents[i] as IHaveSpawnData)?.OnDeserializeSpawnData(reader);
            }
        }

        internal void SetOwnerInternal(Connection connection)
        {
            owner = connection;
            isOwner = connection?.IsMe ?? false;
            isClient = !isServer && !isOwner;

            if (IsServer)
            {
                for (var i = 0; i < netEvents.Length; ++i)
                {
                    (netEvents[i] as IServerOwnerChanged)?.OnServerOwnerAdded(owner);
                }
            }
        }

        internal static NetworkId GetNextNetworkId()
        {
            return new NetworkId(nextNetworkId++);
        }

        private bool TryAddObserver(SmartNetIdentity identity)
        {
            if (identity.Owner != null)
            {
                if (!observers.TryGetValue(identity.Owner, out var ownerObservers))
                {
                    ownerObservers = OwnerObserverPool.Pop();
                    observers[identity.Owner] = ownerObservers;
                }

                return ownerObservers.Add(identity);
            }

            return false;
        }

        private bool TryRemoveObserver(SmartNetIdentity identity)
        {
            if (identity.Owner != null)
            {
                if (observers.TryGetValue(identity.Owner, out var ownerObservers))
                {
                    var removed = ownerObservers.Remove(identity);

                    if (ownerObservers.Count < 1)
                    {
                        observers.Remove(identity.Owner);
                        OwnerObserverPool.Push(ownerObservers);
                    }

                    return removed;
                }
            }

            return false;
        }

        private INetMessage GetCachedMessage(uint messageType)
        {
            var message = handlers.Get(messageType)?.Message;

            if (message == null)
            {
                var type = MessageTypes.GetType(messageType);
                message = (INetMessage)Activator.CreateInstance(type);
                handlers.Register(messageType, message, null);
            }

            return message;
        }

        private void OnDestroy()
        {
            NetworkScene.Destroy(this);
        }

        private void Reset()
        {
            assetId = IdentityLibrary.GetNextPrefabId();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using SmartNet.Messages;

namespace SmartNet
{
    public class NetworkServer
    {
        private static Server serverInstance;
        private static Server Server => serverInstance ?? (serverInstance = new Server());

        private NetworkServer()
        {
        }

        public static bool Active => Server.Active;
        public static int HostId => Server.HostId;
        public static int Port => Server.Port;

        public static event Action StartEvent
        {
            add => Server.StartEvent += value;
            remove => Server.StartEvent -= value;
        }

        public static event Action StopEvent
        {
            add => Server.StopEvent += value;
            remove => Server.StopEvent -= value;
        }

        public static event Action<Connection> NewConnectionEvent
        {
            add => Server.NewConnectionEvent += value;
            remove => Server.NewConnectionEvent -= value;
        }

        public static event Action<Connection> ConnectionDisconnectEvent
        {
            add => Server.ConnectionDisconnectEvent += value;
            remove => Server.ConnectionDisconnectEvent -= value;
        }

        public static void Configure(ServerSettings settings)
        {
            Server.Configure(settings);
        }

        public static void Start(string ip, int port)
        {
            Server.Start(ip, port);
        }

        public static void Stop()
        {
            Server.Stop();
        }

        public static bool SendToAll<T>(T message, int channelId) where T : INetMessage
        {
            return Server.SendToAll(message, channelId);
        }

        public static bool SendToAll(uint messageType, INetMessage message, int channelId)
        {
            return Server.SendToAll(messageType, message, channelId);
        }

        public static bool SendToConnections<T>(IEnumerable<Connection> sendConnections, T msg, int channelId) where T : INetMessage
        {
            return Server.SendToConnections(sendConnections, msg, channelId);
        }

        public static bool SendToConnections(IEnumerable<Connection> sendConnections, INetMessage msg, int channelId)
        {
            return Server.SendToConnections(sendConnections, msg, channelId);
        }

        public static bool SendToConnections(IEnumerable<Connection> sendConnections, uint messageType, INetMessage msg, int channelId)
        {
            return Server.SendToConnections(sendConnections, messageType, msg, channelId);
        }

        public static void RegisterHandler<T>(GenericMessageDelegate<T> handler) where T : INetMessage, new()
        {
            Server.RegisterHandler(handler);
        }

        public static void RegisterHandler(uint messageType, NetworkMessageDelegate handler)
        {
            Server.RegisterHandler(messageType, handler);
        }

        public static void UnregisterHandler<T>() where T : INetMessage
        {
            Server.UnregisterHandler<T>();
        }

        public static void UnregisterHandler(uint messageType)
        {
            Server.UnregisterHandler(messageType);
        }

        public static bool HasHandler<T>() where T : INetMessage
        {
            return Server.HasHandler<T>();
        }

        public static bool HasHandler(uint messageType)
        {
            return Server.HasHandler(messageType);
        }
    }
}
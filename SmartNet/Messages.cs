﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartNet.Messages
{
    public abstract class MessageTypes
    {
        protected static MessageTypes Instance = new DefaultMessageTypes();

        public static void SetMessageType(MessageTypes messageTypes)
        {
            Instance = messageTypes;
        }

        public static uint Count => Instance.MessageCount;
        public static Type[] Types => Instance.AllTypes;
        public static string[] TypeNames => Instance.AllTypeNames;

        public static Type GetType(uint typeId)
        {
            return Instance.GetTypeFromId(typeId);
        }

        public static string GetTypeName(uint typeId)
        {
            return Instance.GetTypeNameFromId(typeId);
        }

        public static uint GetTypeId(Type type)
        {
            return Instance.GetTypeIdFromType(type);
        }

        public static uint GetTypeId<T>() where T : INetMessage
        {
            return Instance.GetTypeIdGeneric<T>();
        }

        public static string GetTypeName<T>() where T : INetMessage
        {
            return Instance.GetTypeNameFromType<T>();
        }

        public abstract uint MessageCount { get; }
        public abstract Type[] AllTypes { get; }
        public abstract string[] AllTypeNames { get; }

        public abstract Type GetTypeFromId(uint typeId);
        public abstract string GetTypeNameFromId(uint typeId);
        public abstract uint GetTypeIdFromType(Type type);
        public abstract uint GetTypeIdGeneric<T>() where T : INetMessage;
        public abstract string GetTypeNameFromType<T>() where T : INetMessage;
    }

    public class DefaultMessageTypes : MessageTypes
    {
        public DefaultMessageTypes()
        {
            const string builtinMessageNamespace = "SmartNet.Messages";

            idToType = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes())
                .Where(x => typeof(INetMessage).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract && x.Namespace == builtinMessageNamespace).OrderBy(x => x.FullName).ToArray();
            var allTypeNames = new string[idToType.Length];

            for (var i = 0; i < idToType.Length; ++i)
            {
                var type = idToType[i];
                var id = (uint)(i + 1);
                TypeToId[type] = id;
                allTypeNames[i] = type.Name;
            }

            if (idToType.Length < Default.BuiltinTypesMinLength)
            {
                Array.Resize(ref idToType, Default.BuiltinTypesMinLength);
            }

            MessageCount = (uint)idToType.Length;
            AllTypes = idToType;
            AllTypeNames = allTypeNames;
        }

        private static readonly Dictionary<Type, uint> TypeToId = new Dictionary<Type, uint>();
        private static Type[] idToType;

        public override uint MessageCount { get; }
        public override Type[] AllTypes { get; }
        public override string[] AllTypeNames { get; }

        public override Type GetTypeFromId(uint typeId)
        {
            var index = typeId - 1;
            return index >= idToType.Length ? null : idToType[index];
        }

        public override string GetTypeNameFromId(uint typeId)
        {
            var index = typeId - 1;
            return index >= AllTypeNames.Length ? null : AllTypeNames[index];
        }

        public override uint GetTypeIdFromType(Type type)
        {
            return TypeToId.TryGetValue(type, out uint messageType) ? messageType : 0u;
        }

        public override uint GetTypeIdGeneric<T>()
        {
            return GetTypeIdFromType(typeof(T));
        }

        public override string GetTypeNameFromType<T>()
        {
            return GetTypeNameFromId(GetTypeIdFromType(typeof(T)));
        }
    }
}
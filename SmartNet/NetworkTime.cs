﻿using System;
using System.Collections.Generic;
using NetworkTransport = UnityEngine.Networking.NetworkTransport;
using SmartNet.Messages;

namespace SmartNet
{
    public static class NetworkTime
    {
        private const int BroadcastDelayMilliseconds = 1000;
        private static int milliseconds, nextServerBroadcastTime, lastReceivedLocalTime, lastReceivedServerTime;
        private static readonly UpdateTime UpdateTime = new UpdateTime();

        public static bool HasReceivedTime { get; private set; }
        public static int StartMilliseconds { get; private set; }

        public static int Milliseconds
        {
            get => milliseconds;
            private set
            {
                milliseconds = value;
                Seconds = milliseconds / 1000f;
            }
        }

        public static float Seconds { get; private set; }
        private static Stack<Action<int>> firstTimeUpdateRequests = new Stack<Action<int>>(12);

        static NetworkTime()
        {
            StartMilliseconds = NetworkTransport.GetNetworkTimestamp();
            Milliseconds = StartMilliseconds;
            nextServerBroadcastTime = Milliseconds + BroadcastDelayMilliseconds;
        }

        public static void AddFirstUpdateCallback(Action<int> callback)
        {
            if (HasReceivedTime)
            {
                callback(Milliseconds);
            }
            else
            {
                firstTimeUpdateRequests.Push(callback);
            }
        }

        public static void Update(bool isServer)
        {
            var now = NetworkTransport.GetNetworkTimestamp();

            if (isServer)
            {
                Milliseconds = now;

                TryClearFirstUpdateCallbacks();

                if (Milliseconds >= nextServerBroadcastTime)
                {
                    UpdateTime.Time = Milliseconds;

                    for (var i = 0; i < Server.Servers.Count; ++i)
                    {
                        Server.Servers[i].SendToAll(UpdateTime, 0);
                    }

                    nextServerBroadcastTime = Milliseconds + BroadcastDelayMilliseconds;
                }
            }
            else
            {
                Milliseconds = lastReceivedServerTime + now - lastReceivedLocalTime;
            }
        }

        public static void ReceiveFirstTimeFromServer(GenericMessageInfo<UpdateFirstTime> info)
        {
            var conn = info.Connection;
            var serverTime = info.Message.Time;
            var delayMs = NetworkTransport.GetRemoteDelayTimeMS(conn.HostId, conn.ConnectionId, serverTime, out var error);
            StartMilliseconds = info.Message.StartTime;
            UpdateClientTime(serverTime, delayMs);

            TryClearFirstUpdateCallbacks();
        }

        public static void ReceiveTimeFromServer(GenericMessageInfo<UpdateTime> info)
        {
            var conn = info.Connection;
            var serverTime = info.Message.Time;
            var delayMs = NetworkTransport.GetRemoteDelayTimeMS(conn.HostId, conn.ConnectionId, serverTime, out var error);
            UpdateClientTime(serverTime, delayMs);
        }

        private static void UpdateClientTime(int serverTime, int delayMs)
        {
            var newTime = serverTime + delayMs;
            Milliseconds = newTime;
            lastReceivedLocalTime = NetworkTransport.GetNetworkTimestamp();
            lastReceivedServerTime = newTime;
        }

        private static void TryClearFirstUpdateCallbacks()
        {
            if (HasReceivedTime)
            {
                return;
            }

            HasReceivedTime = true;

            var time = StartMilliseconds;

            while (firstTimeUpdateRequests.Count > 0)
            {
                firstTimeUpdateRequests.Pop()?.Invoke(time);
            }
        }
    }
}
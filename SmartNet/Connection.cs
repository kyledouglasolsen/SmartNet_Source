﻿using Debug = UnityEngine.Debug;
using NetworkError = UnityEngine.Networking.NetworkError;
using NetworkTransport = UnityEngine.Networking.NetworkTransport;
using System;
using SmartNet.Messages;
using SmartNet.Profiler;

namespace SmartNet
{
    public class Connection : IDisposable
    {
        private readonly bool isServer;
        private readonly ChannelBuffer[] channels;
        private readonly MessageHandlers handlers;
        private readonly MessageInfo messageInfo = new MessageInfo();
        private readonly Writer messageWriter = new Writer();

        private readonly PooledStack<Reader> readers = new PooledStack<Reader>(() => new Reader(), 64);

        public readonly string Ip;
        public readonly int HostId;
        public readonly int ConnectionId;
        public readonly bool IsMe;

        public SmartNetIdentity Player { get; private set; }

        public Connection(string ip, int hostId, int connectionId, ServerSettings serverSettings, MessageHandlers handlers, bool isMe, bool isServer)
        {
            Ip = ip;
            HostId = hostId;
            ConnectionId = connectionId;
            IsMe = isMe;
            this.handlers = handlers ?? new MessageHandlers();
            this.isServer = isServer;

            var channelCount = serverSettings.DefaultConfig.ChannelCount;
            var packetSize = serverSettings.DefaultConfig.PacketSize;

            channels = new ChannelBuffer[channelCount];

            for (var i = 0; i < channelCount; i++)
            {
                var qos = serverSettings.DefaultConfig.Channels[i];
                int actualPacketSize = packetSize;

                if (qos.Qos == QosType.ReliableFragmented || qos.Qos == QosType.UnreliableFragmented)
                {
                    actualPacketSize = serverSettings.DefaultConfig.FragmentSize * 128;
                }

                channels[i] = new ChannelBuffer(this, actualPacketSize, (byte)i, Helpers.IsReliableQoS(qos.Qos), Helpers.IsSequencedQoS(qos.Qos));
            }
        }

        public int GetRTT()
        {
            return NetworkTransport.GetCurrentRTT(HostId, ConnectionId, out _);
        }

        public void Disconnect()
        {
            if (HostId == -1)
            {
                return;
            }

            if (Player != null)
            {
                NetworkScene.Destroy(Player);
            }

            NetworkTransport.Disconnect(HostId, ConnectionId, out var error);
        }

        public void RegisterHandler(uint messageType, NetworkMessageDelegate handler)
        {
            handlers.Register(messageType, handler);
        }

        public void UnregisterHandler(uint messageType)
        {
            handlers.Unregister(messageType);
        }

        public bool HasHandler(uint messageType)
        {
            return handlers.HasHandler(messageType);
        }

        public bool InvokeHandlerNoData(uint messageType)
        {
            return InvokeHandler(messageType, null, 0);
        }

        public bool InvokeHandler(uint messageType, Reader reader, int channelId)
        {
            messageInfo.Initialize(messageType, this, reader, channelId, isServer);
            return InvokeHandler(messageInfo);
        }

        public bool InvokeHandler(MessageInfo info)
        {
            var handler = handlers.Get(info.Type);

            if (handler == null)
            {
                var type = MessageTypes.GetType(info.Type);
                Debug.LogError(type != null ? $"No handler for builtin messageType {type}" : $"No handler for messageType {info.Type}");
                return false;
            }

            handler.Invoke(info);
            return true;
        }

        internal void HandleFragment(Reader reader, int channelId)
        {
            if (channelId < 0 || channelId >= channels.Length)
            {
                return;
            }

            var channel = channels[channelId];
            if (channel.HandleFragment(reader))
            {
                var msgReader = new Reader(channel.FragmentBuffer.AsArray());
                var msgType = msgReader.ReadUIntCompressed();
                InvokeHandler(msgType, msgReader, channelId);
            }
        }

        public void SetPlayer(SmartNetIdentity identity)
        {
            Player = identity;
        }

        public void FlushChannels()
        {
            if (channels == null)
            {
                return;
            }

            for (var i = 0; i < channels.Length; ++i)
            {
                channels[i].CheckInternalBuffer();
            }
        }

        public bool Send(INetMessage message, int channelId)
        {
            return Send(message, channelId, out _);
        }

        public bool Send(INetMessage message, int channelId, out uint byteCount)
        {
            return Send(MessageTypes.GetTypeId(message.GetType()), message, channelId, out byteCount);
        }

        public bool Send<T>(T message, int channelId) where T : INetMessage
        {
            return Send(message, channelId, out _);
        }

        public bool Send<T>(T message, int channelId, out uint byteCount) where T : INetMessage
        {
            messageWriter.SeekZero();
            messageWriter.WriteMessageType<T>();
            messageWriter.Write(message);
            byteCount = messageWriter.Position;

            Statistics.Add<T>(NetworkDirection.Outbound, byteCount);

            return CheckChannel(channelId) && channels[channelId].Send(messageWriter);
        }

        public bool Send(uint messageType, INetMessage message, int channelId)
        {
            return Send(messageType, message, channelId, out _);
        }

        public bool Send(uint messageType, INetMessage message, int channelId, out uint byteCount)
        {
            messageWriter.SeekZero();
            messageWriter.WriteMessageType(messageType);
            messageWriter.Write(message);
            byteCount = messageWriter.Position;

            Statistics.Add(messageType, NetworkDirection.Outbound, byteCount);

            return CheckChannel(channelId) && channels[channelId].Send(messageWriter);
        }

        public bool Send(byte[] bytes, int count, int channelId)
        {
            return CheckChannel(channelId) && channels[channelId].Send(bytes, count);
        }

        public bool Send(Writer writer, int channelId)
        {
            return CheckChannel(channelId) && channels[channelId].Send(writer);
        }

        public bool TransportSend(byte[] bytes, int count, int channelId, out NetworkError error)
        {
            var sent = NetworkTransport.Send(HostId, ConnectionId, channelId, bytes, count, out byte errorByte);
            error = (NetworkError)errorByte;

            if (sent)
            {
                Statistics.AddToBandwidth(NetworkDirection.Outbound, (uint)count);
            }

            return sent;
        }

        public void TransportReceive(byte[] bytes, int count, int channelId)
        {
            var reader = readers.Pop();
            reader.Replace(bytes);
            HandleReader(reader, count, channelId);
            readers.Push(reader);

            Statistics.AddToBandwidth(NetworkDirection.Inbound, (uint)count);
        }

        protected void HandleReader(Reader reader, int count, int channelId)
        {
            while (reader.Position < count)
            {
                var messageType = reader.ReadUIntCompressed();
                var messageReader = readers.Pop();
                messageReader.Replace(reader);

                try
                {
                    InvokeHandler(messageType, messageReader, channelId);
                }
                catch (Exception e)
                {
                    Debug.LogError($"Exception while reading messages: {e}");
                    return;
                }
                finally
                {
                    reader.MovePosition(messageReader.ReadByteCount);
                    readers.Push(messageReader);

                    Statistics.Add(messageType, NetworkDirection.Inbound, reader.ReadByteCount);
                }
            }
        }

        internal static void OnFragment(MessageInfo info)
        {
            info.Connection.HandleFragment(info.Reader, info.ChannelId);
        }

        private bool CheckChannel(int channelId)
        {
            if (channels == null)
            {
                Debug.LogError($"Channels not initialized sending on id {channelId}");
                return false;
            }

            if (channelId < 0 || channelId >= channels.Length)
            {
                Debug.LogError($"Invalid channel when sending buffered data, {channelId}. Current channel count is {channels.Length}");
                return false;
            }

            return true;
        }

        private bool disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed && channels != null)
            {
                for (var i = 0; i < channels.Length; i++)
                {
                    channels[i].Dispose();
                }
            }

            disposed = true;
        }

        ~Connection()
        {
            Dispose(false);
        }
    }
}
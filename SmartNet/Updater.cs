﻿using MonoBehaviour = UnityEngine.MonoBehaviour;
using GameObject = UnityEngine.GameObject;
using HideFlags = UnityEngine.HideFlags;
using Time = UnityEngine.Time;
using System;
using SmartNet.Profiler;
using UnityEngine;

namespace SmartNet
{
    public static class Updater
    {
        //public static Updater Instance = new Updater();

        public static event Action PreUpdateEvent = delegate { }, PostUpdateEvent = delegate { };

//        public static void Init()
//        {
//            if (Instance != null)
//            {
//                return;
//            }
//
//            //Instance = new GameObject("SmartNet.Updater").AddComponent<Updater>();
//            //instance.gameObject.hideFlags = HideFlags.HideAndDontSave;
//            //DontDestroyOnLoad(instance.gameObject);
//        }

        public static void ExternalUpdate()
        {
            if (Server.Servers.Count > 0)
            {
                NetworkTime.Update(true);
            }

            NetworkTime.Update(false);

            PreUpdateEvent();

            Server.UpdateAll();
            Client.UpdateAll();

            PostUpdateEvent();

            Statistics.Tick(Time.time);
        }
    }
}
﻿using ScriptableObject = UnityEngine.ScriptableObject;

namespace SmartNet
{
    public delegate void NetworkMessageDelegate(MessageInfo info);

    public delegate void IdentityMessageDelegate(INetMessage message);

    public delegate void GenericMessageDelegate<T>(GenericMessageInfo<T> netMessage) where T : INetMessage, new();

    public class GenericMessageInfo<T> where T : INetMessage
    {
        public GenericMessageInfo()
        {
        }

        public GenericMessageInfo(uint type, Connection connection, T message)
        {
            Type = type;
            Connection = connection;
            Message = message;
        }

        public uint Type;
        public Connection Connection;
        public T Message;

        private static readonly PooledStack<GenericMessageInfo<T>> Pool = new PooledStack<GenericMessageInfo<T>>(() => new GenericMessageInfo<T>());

        public static GenericMessageInfo<T> Pop(uint type, Connection connection, T message)
        {
            var info = Pool.Pop();
            info.Type = type;
            info.Connection = connection;
            info.Message = message;
            return info;
        }

        public static void Push(GenericMessageInfo<T> info)
        {
            Pool.Push(info);
        }
    }

    public static class Default
    {
        public static bool IsDefaultMessage(uint messageType)
        {
            return BuiltinTypesMinLength >= messageType;
        }

        public const int BuiltinTypesMinLength = 32;
        public const int ConnectionCount = 128;

        private static ConnectionConfig connectionConfig;

        public static ConnectionConfig ConnectionConfig
        {
            get
            {
                if (connectionConfig == null)
                {
                    connectionConfig = new ConnectionConfig();
                    connectionConfig.AddChannel(QosType.Reliable);
                    connectionConfig.AddChannel(QosType.Unreliable);
                }

                return connectionConfig;
            }
        }

        private static ServerSettings serverSettings;

        public static ServerSettings ServerSettings => serverSettings ?? (serverSettings = ScriptableObject.CreateInstance<ServerSettings>().Initialize(ConnectionConfig, ConnectionCount));
    }
}
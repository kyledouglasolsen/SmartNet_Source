﻿using System;
using System.Collections.Generic;

namespace SmartNet
{
    public class PooledStack<T> : Stack<T>
    {
        private readonly Func<T> createCallback;

        public PooledStack(Func<T> createCallback, int preAllocate = 0)
        {
            this.createCallback = createCallback;

            if (createCallback != null)
            {
                for (var i = 0; i < preAllocate; ++i)
                {
                    Push(createCallback());
                }
            }
        }

        public new T Pop()
        {
            return Count < 1 ? createCallback() : base.Pop();
        }
    }
}
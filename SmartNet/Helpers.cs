﻿using Debug = UnityEngine.Debug;
using System;
using System.Net;
using System.Net.Sockets;

namespace SmartNet
{
    public static class Helpers
    {
        public static void GetIpOrHostname(Client client, string ip)
        {
            var hostnameOrIp = ip;

            if (UnityEngine.Application.platform == UnityEngine.RuntimePlatform.WebGLPlayer)
            {
                client.Ip = hostnameOrIp;
                client.State = ConnectState.Resolved;
            }
            else if (ip.Equals("127.0.0.1") || ip.Equals("localhost"))
            {
                client.Ip = "127.0.0.1";
                client.State = ConnectState.Resolved;
            }
            else if (ip.IndexOf(":") != -1 && IsValidIpV6(ip))
            {
                client.Ip = ip;
                client.State = ConnectState.Resolved;
            }
            else
            {
                client.State = ConnectState.Resolving;
                Dns.BeginGetHostAddresses(hostnameOrIp, GetHostAddressesCallback, new ConnectRequest(hostnameOrIp, client));
            }
        }

        public static bool IsValidIpV6(string address)
        {
            foreach (var c in address)
            {
                if (c == ':' || c >= '0' && c <= '9' || c >= 'a' && c <= 'f' || c >= 'A' && c <= 'F')
                {
                    continue;
                }

                return false;
            }

            return true;
        }

        public static bool IsSequencedQoS(QosType qos)
        {
            return (qos == QosType.ReliableSequenced || qos == QosType.UnreliableSequenced || qos == QosType.ReliableFragmentedSequenced || qos == QosType.UnreliableFragmentedSequenced);
        }

        public static bool IsReliableQoS(QosType qos)
        {
            return qos == QosType.Reliable || qos == QosType.ReliableFragmented || qos == QosType.ReliableSequenced || qos == QosType.ReliableStateUpdate ||
                   qos == QosType.ReliableFragmentedSequenced;
        }
        
        internal static void GetHostAddressesCallback(IAsyncResult ar)
        {
            try
            {
                var ip = Dns.EndGetHostAddresses(ar);
                var request = (ConnectRequest)ar.AsyncState;
                var client = request.Client;

                if (ip.Length == 0)
                {
                    Debug.LogError($"DNS lookup failed for: {request.RequestedHost}");
                    client.State = ConnectState.Failed;
                    return;
                }

                client.Ip = ip[0].ToString();
                client.State = ConnectState.Resolved;
                Debug.Log($"Async DNS Result: {client.Ip} for {request.RequestedHost}:{client.Ip}");
            }
            catch (SocketException e)
            {
                var client = (Client)ar.AsyncState;
                Debug.LogError($"DNS resolution failed: {e.ErrorCode}");
                Debug.Log($"Exception: {e}");
                client.State = ConnectState.Failed;
            }
        }

        private class ConnectRequest
        {
            public ConnectRequest(string requestedHost, Client client)
            {
                RequestedHost = requestedHost;
                Client = client;
            }

            public readonly string RequestedHost;
            public readonly Client Client;
        }
    }
}
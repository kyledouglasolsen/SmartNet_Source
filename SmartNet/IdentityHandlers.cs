﻿using System.Collections.Generic;
using SmartNet.Messages;

namespace SmartNet
{
    public class IdentityHandlers
    {
        private readonly Dictionary<uint, CacheCallback> handlers = new Dictionary<uint, CacheCallback>();

        public void Register(uint messageType, INetMessage cachedMessage, IdentityMessageDelegate handler)
        {
            handlers[messageType] = new CacheCallback(cachedMessage, handler);
        }

        public void Register<T>(T cachedMessage, IdentityMessageDelegate handler) where T : INetMessage
        {
            handlers[MessageTypes.GetTypeId<T>()] = new CacheCallback(cachedMessage, handler);
        }

        public void Unregister(uint messageType)
        {
            handlers.Remove(messageType);
        }

        public void Unregister<T>() where T : INetMessage
        {
            handlers.Remove(MessageTypes.GetTypeId<T>());
        }

        public bool HasHandler<T>() where T : INetMessage
        {
            return HasHandler(MessageTypes.GetTypeId<T>());
        }

        public bool HasHandler(uint messageType)
        {
            return handlers.ContainsKey(messageType);
        }

        public CacheCallback Get<T>() where T : INetMessage
        {
            return Get(MessageTypes.GetTypeId<T>());
        }

        public CacheCallback Get(uint messageType)
        {
            return handlers.TryGetValue(messageType, out var handler) ? handler : null;
        }

        public void Clear()
        {
            handlers.Clear();
        }
    }

    public class CacheCallback
    {
        public IdentityMessageDelegate Handler { get; private set; }
        public INetMessage Message { get; private set; }

        public CacheCallback(INetMessage message, IdentityMessageDelegate handler)
        {
            Message = message;
            Handler = handler;
        }

        public void SetMessage(INetMessage message)
        {
            Message = message;
        }
    }
}
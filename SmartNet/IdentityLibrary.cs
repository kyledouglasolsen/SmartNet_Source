﻿using Debug = UnityEngine.Debug;
using ScriptableObject = UnityEngine.ScriptableObject;
using System.Collections.Generic;

namespace SmartNet
{
    public class IdentityLibrary : ScriptableObject
    {
        public static IdentityLibrary Ins { get; private set; }

        public SmartNetIdentity Player;
        public List<SmartNetIdentity> Library = new List<SmartNetIdentity>();

        private void OnEnable()
        {
            Ins = this;
        }

        public static bool Exists()
        {
            return Ins != null;
        }

        public static bool Add(SmartNetIdentity identity)
        {
            if (!Ins.Library.Contains(identity))
            {
                Ins.Library.Add(identity);
                return true;
            }

            return false;
        }

        public static bool Remove(SmartNetIdentity identity)
        {
            return Ins.Library.Remove(identity);
        }

        public static bool Contains(SmartNetIdentity identity)
        {
            return Ins.Library.Contains(identity);
        }

        public static bool Contains(uint assetId)
        {
            for (var i = 0; i < Ins.Library.Count; ++i)
            {
                if (Ins.Library[i]?.AssetId == assetId)
                {
                    return true;
                }
            }

            return false;
        }

        public static SmartNetIdentity Get(uint assetId)
        {
            for (var i = 0; i < Ins.Library.Count; ++i)
            {
                if (Ins.Library[i]?.AssetId == assetId)
                {
                    return Ins.Library[i];
                }
            }

            return null;
        }

        public static void RemoveNullReferences()
        {
            Ins?.Library?.RemoveAll(x => x == null);
        }

        public static uint GetNextPrefabId()
        {
            for (var nextId = 0u; nextId < uint.MaxValue; ++nextId)
            {
                if (!Contains(nextId))
                {
                    return nextId;
                }
            }

            Debug.LogError($"Exceeded max Prefab count of {uint.MaxValue}. Critical failure!");
            return 0u;
        }
    }
}
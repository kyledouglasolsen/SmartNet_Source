﻿using Debug = UnityEngine.Debug;
using NetworkError = UnityEngine.Networking.NetworkError;
using System;

namespace SmartNet
{
    internal struct ChannelPacket
    {
        private int position;
        private byte[] buffer;
        private bool isReliable;

        public ChannelPacket(int packetSize, bool isReliable)
        {
            position = 0;
            buffer = new byte[packetSize];
            this.isReliable = isReliable;
        }

        public void Reset()
        {
            position = 0;
        }

        public bool IsEmpty()
        {
            return position == 0;
        }

        public void Write(byte[] bytes, int numBytes)
        {
            Array.Copy(bytes, 0, buffer, position, numBytes);
            position += numBytes;
        }

        public bool HasSpace(int numBytes)
        {
            return position + numBytes <= buffer.Length;
        }

        public bool SendToTransport(Connection conn, int channelId)
        {
            var result = true;

            if (!conn.TransportSend(buffer, (ushort)position, channelId, out var error))
            {
                if (isReliable && error == NetworkError.NoResources)
                {
                }
                else
                {
                    Debug.LogError($"Failed to send internal buffer channel {channelId} bytesToSend: {position}");
                    result = false;
                }
            }

            if (error != NetworkError.Ok)
            {
                if (isReliable && error == NetworkError.NoResources)
                {
                    return false;
                }

                Debug.LogError($"Send Error: {error} channel: {channelId} bytesToSend: {position}");
                result = false;
            }

            position = 0;
            return result;
        }
    }
}
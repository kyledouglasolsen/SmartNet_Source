﻿using SerializeField = UnityEngine.SerializeField;
using System;
using System.Collections.Generic;

namespace SmartNet
{
    [Serializable]
    public class ConnectionConfig
    {
        private const int MinPacketSize = 128;

        [SerializeField] private List<ChannelQos> channels = new List<ChannelQos>()
        {
            new ChannelQos(QosType.Reliable),
            new ChannelQos(QosType.Unreliable)
        };

        [SerializeField] private ushort packetSize;
        [SerializeField] private ushort fragmentSize;
        [SerializeField] private uint resendTimeout;
        [SerializeField] private uint disconnectTimeout;
        [SerializeField] private uint connectTimeout;
        [SerializeField] private uint minUpdateTimeout;
        [SerializeField] private uint pingTimeout;
        [SerializeField] private uint reducedPingTimeout;
        [SerializeField] private uint allCostTimeout;
        [SerializeField] private byte networkDropThreshold;
        [SerializeField] private byte overflowDropThreshold;
        [SerializeField] private byte maxConnectionAttempt;
        [SerializeField] private uint ackDelay;
        [SerializeField] private uint sendDelay;
        [SerializeField] private ushort maxCombinedReliableMessageSize;
        [SerializeField] private ushort maxCombinedReliableMessageCount;
        [SerializeField] private ushort maxSentMessageQueueSize;
        [SerializeField] private ConnectionAcksType acksType;
        [SerializeField] private uint initialBandwidth;
        [SerializeField] private float bandwidthPeakFactor;
        [SerializeField] private uint udpSocketReceiveBufferMaxSize;

        /// <summary>
        /// Defines maximum packet size (in bytes) (including payload and all header). Packet can contain multiple messages inside. Default value = 1500. 
        /// Note that this default value is suitable for local testing only. Usually you should change this value; a recommended setting for PC or mobile is 1470. 
        /// For games consoles this value should probably be less than ~1100. Wrong size definition can cause packet dropping.
        /// </summary>
        public ushort PacketSize
        {
            get => packetSize;
            set => packetSize = value;
        }

        /// <summary>
        /// Defines the fragment size for fragmented messages (for QOS: ReliableFragmented and UnreliableFragmented). Default value = 500. 
        /// Under fragmented quality of service modes, the original message is split into fragments (up to 64) of up to FragmentSize bytes each. 
        /// The fragment size depends on the frequency and size of reliable messages sent. 
        /// Each reliable message potentially could be re-sent, so you need to choose a fragment size less than the remaining free space in a UDP packet after retransmitted reliable messages are added to the packet.
        /// For example, if PacketSize is 1440 bytes, and a reliable message's average size is 200 bytes, it would be wise to set this parameter to 900 – 1000 bytes.
        /// </summary>
        public ushort FragmentSize
        {
            get => fragmentSize;
            set => fragmentSize = value;
        }

        /// <summary>
        /// Defines the maximum wait time in milliseconds before the "not acknowledged" message is re-sent. Default value = 1200. 
        /// It does not make a lot of sense to wait for acknowledgment forever. This parameter sets an upper time limit at which point reliable messages are re-sent.
        /// </summary>
        public uint ResendTimeout
        {
            get => resendTimeout;
            set => resendTimeout = value;
        }

        /// <summary>
        /// Defines the timeout in milliseconds before a connection is considered to have been disconnected. Default value = 2000. 
        /// Unity Multiplayer defines conditions under which a connection is considered as disconnected. Disconnection can happen for the following reasons: 
        /// (1) A disconnection request was received. 
        /// (2) The connection has not received any traffic at all for a time longer than DisconnectTimeout.
        /// (Note that live connections receive regular keep-alive packets, so in this case "no traffic" means not only no user traffic but also absence of any keep-alive traffic as well).
        /// (3) Flow control determines that the time between sending packets is longer than DisconnectTimeout. Keep-alive packets are regularly delivered from peers and contain statistical information. This information includes values of packet loss due to network and peer overflow conditions. Setting NetworkDropThreshold and OverflowDropThreshold defines thresholds for flow control which can decrease packet frequency. When the time before sending the next packet is longer than DisconnectTimeout, the connection will be considered as disconnected and a disconnect event is received.</para>
        /// </summary>
        public uint DisconnectTimeout
        {
            get => disconnectTimeout;
            set => disconnectTimeout = value;
        }

        /// <summary>
        /// Timeout in ms which library will wait before it will send another connection request.
        /// </summary>
        public uint ConnectTimeout
        {
            get => connectTimeout;
            set => connectTimeout = value;
        }

        /// <summary>
        /// Defines minimum time in milliseconds between sending packets. This duration may be automatically increased if required by flow control. Default value = 10. 
        /// When Send() is called, Unity Multiplayer won’t send the message immediately. 
        /// Instead, once every SendTimeout milliseconds each connection is checked to see if it has something to send. While initial and minimal send timeouts can be set,
        /// these may be increased internally due to network conditions or buffer overflows.
        /// </summary>
        public uint MinUpdateTimeout
        {
            get => minUpdateTimeout;
            set
            {
                if (value == 0u)
                {
                    throw new ArgumentOutOfRangeException($"Minimal update timeout must be greater than 0");
                }

                minUpdateTimeout = value;
            }
        }

        /// <summary>
        /// Defines the duration in milliseconds between keep-alive packets, also known as pings. Default value = 500. 
        /// The ping frequency should be long enough to accumulate good statistics and short enough to compare with DisconnectTimeout. 
        /// A good guideline is to have more than 3 pings per disconnect timeout, and more than 5 messages per ping. 
        /// For example, with a DisconnectTimeout of 2000ms, a PingTimeout of 500ms works well.
        /// </summary>
        public uint PingTimeout
        {
            get => pingTimeout;
            set => pingTimeout = value;
        }

        public uint ReducedPingTimeout
        {
            get => reducedPingTimeout;
            set => reducedPingTimeout = value;
        }

        /// <summary>
        /// Defines the timeout in milliseconds after which messages sent via the AllCost channel will be re-sent without waiting for acknowledgment. Default value = 20 ms. 
        /// AllCost delivery quality of service (QOS) is a special QOS for delivering game-critical information, such as when the game starts, or when bullets are shot. 
        /// Due to packets dropping, sometimes reliable messages cannot be delivered and need to be re-sent. 
        /// Reliable messages will re-sent after RTT+Delta time, (RTT is round trip time) where RTT is a dynamic value and can reach couple of hundred milliseconds. 
        /// For the AllCost delivery channel this timeout can be user-defined to force game critical information to be re-sent.
        /// </summary>
        public uint AllCostTimeout
        {
            get => allCostTimeout;
            set => allCostTimeout = value;
        }

        /// <summary>
        /// Defines the percentage (from 0 to 100) of packets that need to be dropped due to network conditions before the SendUpdate timeout is automatically increased (and send rate is automatically decreased). Default value = 5.
        /// To avoid receiver overflow, Unity Multiplayer supports flow control. Each ping packet sent between connected peers contains two values: 
        /// (1) Packets lost due to network conditions. 
        /// (2) Packets lost because the receiver does not have free space in its incoming buffers. 
        /// Like OverflowDropThreshold, both values are reported in percent. Use NetworkDropThreshold and OverflowDropThreshold to set thresholds for these values. 
        /// If a value reported in the ping packet exceeds the corresponding threshold, Unity Multiplayer increases the sending timeout for packets up to a maximum value of DisconnectTimeout. 
        /// Note: wireless networks usually exhibit 5% or greater packet loss. For wireless networks it is advisable to use a NetworkDropThreshold of 40-50%.
        /// </summary>
        public byte NetworkDropThreshold
        {
            get => networkDropThreshold;
            set => networkDropThreshold = value;
        }

        /// <summary>
        /// Defines the percentage (from 0 to 100) of packets that need to be dropped due to lack of space in internal buffers before the SendUpdate timeout is automatically increased (and send rate is automatically decreased). Default value = 5. 
        /// To avoid receiver overflow, Unity Multiplayer supports flow control. Each ping packet sent between connected peers contains two values: 
        /// (1) Packets lost due to network conditions. 
        /// (2) Packets lost because the receiver does not have free space in its incoming buffers. 
        /// Like NetworkDropThreshold, both values are reported in percent. Use NetworkDropThreshold and OverflowDropThreshold to set thresholds for these values. If a value reported in the ping packet exceeds the corresponding threshold, Unity Multiplayer increases the sending timeout for packets up to a maximum value of DisconnectTimeout.
        /// Note: wireless networks usually exhibit 5% or greater packet loss. For wireless networks it is advisable to use a NetworkDropThreshold of 40-50%.
        /// </summary>
        public byte OverflowDropThreshold
        {
            get => overflowDropThreshold;
            set => overflowDropThreshold = value;
        }

        /// <summary>
        /// Defines the maximum number of times Unity Multiplayer will attempt to send a connection request without receiving a response before it reports that it cannot establish a connection. Default value = 10.
        /// </summary>
        public byte MaxConnectionAttempt
        {
            get => maxConnectionAttempt;
            set => maxConnectionAttempt = value;
        }

        /// <summary>
        /// Defines the duration in milliseconds that the receiver waits for before it sends an acknowledgment back without waiting for any data payload. Default value = 33. 
        /// Network clients that send data to a server may do so using many different quality of service (QOS) modes, 
        /// some of which (reliable modes) expect the server to send back acknowledgment of receipt of data sent. 
        /// Servers must periodically acknowledge data packets received over channels with reliable QOS modes by sending packets containing acknowledgment data (also known as "acks") back to the client. 
        /// If the server were to send an acknowledgment immediately after receiving each packet from the client there would be significant overhead (the acknowledgment is a 32 or 64 bit integer, 
        /// which is very small compared to the whole size of the packet which also contains the IP and the UDP header). 
        /// AckDelay allows the server some time to accumulate a list of received reliable data packets to acknowledge,
        /// and decreases traffic overhead by combining many acknowledgments into a single packet.
        /// </summary>
        public uint AckDelay
        {
            get => ackDelay;
            set => ackDelay = value;
        }

        /// <summary>
        /// Gets or sets the delay in milliseconds after a call to Send() before packets are sent.
        /// During this time, new messages may be combined in queued packets. Default value: 10ms.
        /// </summary>
        public uint SendDelay
        {
            get => sendDelay;
            set => sendDelay = value;
        }

        /// <summary>
        /// Defines the maximum size in bytes of a reliable message which is considered small enough to include in a combined message. Default value = 100.  
        /// Since each message sent to a server contains IP information and a UDP header, 
        /// duplicating this information for every message sent can be inefficient in the case where there are many small messages being sent frequently. 
        /// Many small reliable messages can be combined into one longer reliable message, saving space in the waiting buffer. 
        /// Unity Multiplayer will automatically combine up to MaxCombinedReliableMessageCount small messages into one message. 
        /// To qualify as a small message, the data payload of the message should not be greater than MaxCombinedReliableMessageSize.
        /// </summary>
        public ushort MaxCombinedReliableMessageSize
        {
            get => maxCombinedReliableMessageSize;
            set => maxCombinedReliableMessageSize = value;
        }

        /// <summary>
        /// Defines the maximum number of small reliable messages that can be included in one combined message. Default value = 10. 
        /// Since each message sent to a server contains IP information and a UDP header, 
        /// duplicating this information for every message sent can be inefficient in the case where there are many small messages being sent frequently. 
        /// Many small reliable messages can be combined into one longer reliable message, saving space in the waiting buffer. 
        /// Unity Multiplayer will automatically combine up to MaxCombinedReliableMessageCount small messages into one message. 
        /// To qualify as a small message, the data payload of the message should not be greater than MaxCombinedReliableMessageSize.
        /// </summary>
        public ushort MaxCombinedReliableMessageCount
        {
            get => maxCombinedReliableMessageCount;
            set => maxCombinedReliableMessageCount = value;
        }

        /// <summary>
        /// Defines maximum number of messages that can be held in the queue for sending. Default value = 128. 
        /// This buffer serves to smooth spikes in traffic and decreases network jitter. If the queue is full, a NoResources error will result from any calls to Send(). 
        /// Setting this value greater than around 300 is likely to cause significant delaying of message delivering and can make game unplayable.
        /// </summary>
        public ushort MaxSentMessageQueueSize
        {
            get => maxSentMessageQueueSize;
            set => maxSentMessageQueueSize = value;
        }

        /// <summary>
        /// Determines the size of the buffer used to store reliable messages that are waiting for acknowledgment. 
        /// It can be set to Acks32, Acks64, Acks96, or Acks128. Depends of this setting buffer can hold 32, 64, 96, or 128 messages. Default value = Ack32. 
        /// Messages sent on reliable quality of service channels are stored in a special buffer while they wait for acknowledgment from the peer. 
        /// This buffer can be either 32, 64, 96 or 128 positions long. It is recommended to begin with this value set to Ack32, which defines a buffer up to 32 messages in size.
        /// If you receive NoResources errors often when you send reliable messages, change this value to the next possible size.
        /// </summary>
        public ConnectionAcksType AcksType
        {
            get => acksType;
            set => acksType = value;
        }

        /// <summary>
        /// Gets or sets the bandwidth in bytes per second that can be used by Unity Multiplayer. No traffic over this limit is allowed. 
        /// Unity Multiplayer may internally reduce the bandwidth it uses due to flow control. The default value is 1500MB/sec (1,536,000 bytes per second). 
        /// The default value is intentionally a large number to allow all traffic to pass without delay.
        /// </summary>
        public uint InitialBandwidth
        {
            get => initialBandwidth;
            set => initialBandwidth = value;
        }

        /// <summary>
        /// Defines, when multiplied internally by InitialBandwidth, the maximum bandwidth that can be used under burst conditions.
        /// </summary>
        public float BandwidthPeakFactor
        {
            get => bandwidthPeakFactor;
            set => bandwidthPeakFactor = value;
        }

        /// <summary>
        /// Defines the size in bytes of the receiving buffer for UDP sockets. It is useful to set this parameter equal to the maximum size of a fragmented message.
        /// Default value is OS specific (usually 8kb).
        /// </summary>
        public uint UdpSocketReceiveBufferMaxSize
        {
            get => udpSocketReceiveBufferMaxSize;
            set => udpSocketReceiveBufferMaxSize = value;
        }

        /// <summary>
        /// (Read Only) The number of channels in the current configuration.
        /// </summary>
        public int ChannelCount => channels.Count;

        /// <summary>
        /// The list of channels belonging to the current configuration.  
        /// Note: any ConnectionConfig passed as a parameter to a function in Unity Multiplayer is deep copied (that is, an entirely new copy is made, with no references to the original).
        /// </summary>
        public List<ChannelQos> Channels => channels;

        public ConnectionConfig()
        {
            channels = new List<ChannelQos>()
            {
                new ChannelQos(QosType.Reliable),
                new ChannelQos(QosType.Unreliable)
            };

            packetSize = 1440;
            fragmentSize = 500;
            resendTimeout = 1200U;
            disconnectTimeout = 2000U;
            connectTimeout = 2000U;
            minUpdateTimeout = 10U;
            pingTimeout = 500U;
            reducedPingTimeout = 100U;
            allCostTimeout = 20U;
            networkDropThreshold = 5;
            overflowDropThreshold = 5;
            maxConnectionAttempt = 10;
            ackDelay = 33U;
            sendDelay = 10U;
            maxCombinedReliableMessageSize = 100;
            maxCombinedReliableMessageCount = 10;
            maxSentMessageQueueSize = 512;
            acksType = ConnectionAcksType.Acks32;
            initialBandwidth = 0U;
            bandwidthPeakFactor = 2f;
            udpSocketReceiveBufferMaxSize = 0U;
        }

        /// <summary>
        /// Create a new copy of connection config.
        /// </summary>
        /// <param name="config">Connection config.</param>
        public ConnectionConfig(ConnectionConfig config)
        {
            if (config == null)
            {
                throw new NullReferenceException($"{nameof(config)} is not defined");
            }

            packetSize = config.packetSize;
            fragmentSize = config.fragmentSize;
            resendTimeout = config.resendTimeout;
            disconnectTimeout = config.disconnectTimeout;
            connectTimeout = config.connectTimeout;
            minUpdateTimeout = config.minUpdateTimeout;
            pingTimeout = config.pingTimeout;
            reducedPingTimeout = config.reducedPingTimeout;
            allCostTimeout = config.allCostTimeout;
            networkDropThreshold = config.networkDropThreshold;
            overflowDropThreshold = config.overflowDropThreshold;
            maxConnectionAttempt = config.maxConnectionAttempt;
            ackDelay = config.ackDelay;
            sendDelay = config.sendDelay;
            maxCombinedReliableMessageSize = config.MaxCombinedReliableMessageSize;
            maxCombinedReliableMessageCount = config.maxCombinedReliableMessageCount;
            maxSentMessageQueueSize = config.maxSentMessageQueueSize;
            acksType = config.acksType;
            initialBandwidth = config.initialBandwidth;

            if (initialBandwidth == 0u)
            {
                initialBandwidth = packetSize * 1000u / minUpdateTimeout;
            }

            bandwidthPeakFactor = config.bandwidthPeakFactor;
            udpSocketReceiveBufferMaxSize = config.udpSocketReceiveBufferMaxSize;

            foreach (var channel in config.channels)
            {
                channels.Add(new ChannelQos(channel));
            }
        }

        /// <summary>
        /// Validate parameters of connection config. Will throw exceptions if parameters are incorrect.
        /// </summary>
        /// <param name="config">Connection config.</param>
        public static void Validate(ConnectionConfig config)
        {
            if (config.packetSize < MinPacketSize)
            {
                throw new ArgumentOutOfRangeException($"PacketSize must be greater than {MinPacketSize}");
            }

            if (config.fragmentSize >= config.packetSize - MinPacketSize)
            {
                throw new ArgumentOutOfRangeException($"FragmentSize must be less than {config.packetSize - MinPacketSize}");
            }

            if (config.channels.Count > byte.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(config.channels), "Channels number must be less than 256");
            }
        }

        /// <summary>
        /// Adds a new channel to the configuration and returns the unique id of that channel.
        /// Channels are logical delimiters of traffic between peers. Every time you send data to a peer, you should use two ids: connection id and channel id. 
        /// Channels are not only logically separate traffic but could each be configured with a different quality of service (QOS). 
        /// In the example below, a configuration is created containing two channels with Unreliable and Reliable QOS types. This configuration is then used for sending data.
        /// </summary>
        /// <param name="value">Channel quality of service type</param>
        /// <returns>Channel Id.</returns>
        public byte AddChannel(QosType value)
        {
            if (channels.Count > byte.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(channels), "Channels number must be less than 256");
            }

            if (!Enum.IsDefined(typeof(QosType), value))
            {
                throw new ArgumentOutOfRangeException($"QOS type {value} doesn't exist");
            }

            channels.Add(new ChannelQos(value));

            return (byte)(channels.Count - 1);
        }

        /// <summary>
        /// Return the QoS set for the given channel or throw an out of range exception.
        /// </summary>
        /// <param name="id">Index in array.</param>
        /// <returns>Channel QoS.</returns>
        public QosType GetChannel(byte id)
        {
            if (id >= channels.Count)
            {
                throw new ArgumentOutOfRangeException(nameof(id), $"requested index greater than maximum channels count");
            }

            return (QosType)channels[id].Qos;
        }

        internal UnityEngine.Networking.ConnectionConfig ToConnectionConfig()
        {
            var config = new UnityEngine.Networking.ConnectionConfig()
            {
                PacketSize = packetSize,
                FragmentSize = fragmentSize,
                ResendTimeout = resendTimeout,
                DisconnectTimeout = disconnectTimeout,
                ConnectTimeout = connectTimeout,
                MinUpdateTimeout = minUpdateTimeout,
                PingTimeout = pingTimeout,
                ReducedPingTimeout = reducedPingTimeout,
                AllCostTimeout = allCostTimeout,
                NetworkDropThreshold = networkDropThreshold,
                OverflowDropThreshold = overflowDropThreshold,
                MaxConnectionAttempt = maxConnectionAttempt,
                AckDelay = ackDelay,
                SendDelay = sendDelay,
                MaxCombinedReliableMessageSize = MaxCombinedReliableMessageSize,
                MaxCombinedReliableMessageCount = maxCombinedReliableMessageCount,
                MaxSentMessageQueueSize = maxSentMessageQueueSize,
                AcksType = (UnityEngine.Networking.ConnectionAcksType)acksType,
                InitialBandwidth = initialBandwidth,
                BandwidthPeakFactor = bandwidthPeakFactor,
                UdpSocketReceiveBufferMaxSize = udpSocketReceiveBufferMaxSize,
                SSLCAFilePath = string.Empty,
                SSLCertFilePath = string.Empty,
                SSLPrivateKeyFilePath = string.Empty,
                UsePlatformSpecificProtocols = false,
                WebSocketReceiveBufferMaxSize = 0
            };

            foreach (var channel in channels)
            {
                config.Channels.Add(new UnityEngine.Networking.ChannelQOS((UnityEngine.Networking.QosType)channel.Qos));
            }

            return config;
        }
    }
}
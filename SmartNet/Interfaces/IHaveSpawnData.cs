﻿namespace SmartNet
{
    public interface IHaveSpawnData : INetEvent
    {
        void OnSerializeSpawnData(Writer writer);
        void OnDeserializeSpawnData(Reader reader);
    }
}
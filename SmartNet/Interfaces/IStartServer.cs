﻿namespace SmartNet
{
    public interface IStartServer : INetEvent
    {
        void OnStartServer();
    }
}
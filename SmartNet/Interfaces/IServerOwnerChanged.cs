﻿namespace SmartNet
{
    public interface IServerOwnerChanged : INetEvent
    {
        void OnServerOwnerRemoved(Connection owner);
        void OnServerOwnerAdded(Connection owner);
    }
}
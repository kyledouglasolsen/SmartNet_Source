﻿namespace SmartNet
{
    public interface IStopOwner : INetEvent
    {
        void OnStopOwner();
    }
}
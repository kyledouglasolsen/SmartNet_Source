﻿namespace SmartNet
{
    public interface IStartClient : INetEvent
    {
        void OnStartClient();
    }
}
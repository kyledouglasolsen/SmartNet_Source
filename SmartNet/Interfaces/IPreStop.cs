﻿namespace SmartNet
{
    public interface IPreStop : INetEvent
    {
        void OnPreStop();
    }
}
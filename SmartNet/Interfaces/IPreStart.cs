﻿namespace SmartNet
{
    public interface IPreStart : INetEvent
    {
        void OnPreStart();
    }
}
﻿namespace SmartNet
{
    public interface IStopClient : INetEvent
    {
        void OnStopClient();
    }
}
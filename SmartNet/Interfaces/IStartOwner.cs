﻿namespace SmartNet
{
    public interface IStartOwner : INetEvent
    {
        void OnStartOwner();
    }
}
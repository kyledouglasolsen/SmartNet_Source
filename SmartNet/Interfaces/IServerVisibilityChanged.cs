﻿namespace SmartNet
{
    public interface IServerVisibilityChanged : INetEvent
    {
        void OnAddVisible(SmartNetIdentity identity);
        void OnRemoveVisible(SmartNetIdentity identity);
    }
}
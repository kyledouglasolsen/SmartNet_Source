﻿namespace SmartNet
{
    public interface IStopServer : INetEvent
    {
        void OnStopServer();
    }
}
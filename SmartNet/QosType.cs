﻿namespace SmartNet
{
    public enum QosType
    {
        Unreliable = UnityEngine.Networking.QosType.Unreliable,
        UnreliableFragmented = UnityEngine.Networking.QosType.UnreliableFragmented,
        UnreliableSequenced = UnityEngine.Networking.QosType.UnreliableSequenced,
        Reliable = UnityEngine.Networking.QosType.Reliable,
        ReliableFragmented = UnityEngine.Networking.QosType.ReliableFragmented,
        ReliableSequenced = UnityEngine.Networking.QosType.ReliableSequenced,
        StateUpdate = UnityEngine.Networking.QosType.StateUpdate,
        ReliableStateUpdate = UnityEngine.Networking.QosType.ReliableStateUpdate,
        AllCostDelivery = UnityEngine.Networking.QosType.AllCostDelivery,
        UnreliableFragmentedSequenced = UnityEngine.Networking.QosType.UnreliableFragmentedSequenced,
        ReliableFragmentedSequenced = UnityEngine.Networking.QosType.ReliableFragmentedSequenced
    }
}
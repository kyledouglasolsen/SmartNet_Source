﻿namespace SmartNet
{
    public class MessageInfo
    {
        public uint Type;
        public Connection Connection;
        public Reader Reader;
        public int ChannelId;
        public bool IsServer;

        public MessageInfo()
        {
        }

        public MessageInfo(uint type, Connection connection, Reader reader, int channelId, bool isServer)
        {
            Initialize(type, connection, reader, channelId, isServer);
        }

        public void Initialize(uint type, Connection connection, Reader reader, int channelId, bool isServer)
        {
            Type = type;
            Connection = connection;
            Reader = reader;
            ChannelId = channelId;
            IsServer = isServer;
        }

        public T ReadMessage<T>() where T : INetMessage, new()
        {
            var message = MessageCache<T>.Pop();

            try
            {
                message.OnDeserialize(Reader);
            }
            finally
            {
                MessageCache<T>.Push(message);
            }

            return message;
        }

        public void ReadMessage<T>(T msg) where T : INetMessage
        {
            msg.OnDeserialize(Reader);
        }
    }
}
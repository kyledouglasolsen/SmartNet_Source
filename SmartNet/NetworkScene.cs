﻿using System;
using SmartNet.Messages;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SmartNet
{
    public static class NetworkScene
    {
        public static readonly KnownIdentities KnownIdentities = new KnownIdentities();

        public static event Action<SmartNetIdentity> PrePlayerSpawnEvent = delegate { };
        public static event Action<SmartNetIdentity> PlayerSpawnEvent = delegate { };

        static NetworkScene()
        {
            NetworkServer.StopEvent += () => { KnownIdentities.Clear(); };
        }

        public static bool Add(SmartNetIdentity identity)
        {
            return KnownIdentities.Add(identity);
        }

        public static bool Remove(SmartNetIdentity identity)
        {
            return KnownIdentities.Remove(identity);
        }

        public static SmartNetIdentity InstantiatePlayer(Connection owner, Vector3 position, Quaternion rotation, Reader reader = null)
        {
            return Instantiate(owner, SmartNetIdentity.GetNextNetworkId(), IdentityLibrary.Ins.Player, position, rotation, reader);
        }

        public static SmartNetIdentity InstantiatePlayer(Connection owner, NetworkId networkId, Vector3 position, Quaternion rotation, Reader reader = null)
        {
            return Instantiate(owner, networkId, IdentityLibrary.Ins.Player, position, rotation, reader);
        }

        public static SmartNetIdentity Instantiate(AssetSpawn message, Reader reader = null)
        {
            return Instantiate(null, message.NetworkId, message.AssetId, message.Position, message.Rotation, reader);
        }

        public static SmartNetIdentity Instantiate(Connection owner, AssetSpawn message, Reader reader = null)
        {
            return Instantiate(owner, message.NetworkId, message.AssetId, message.Position, message.Rotation, reader);
        }

        public static SmartNetIdentity Instantiate(NetworkId networkId, uint assetId, Vector3 position, Quaternion rotation, Reader reader = null)
        {
            return Instantiate(null, networkId, IdentityLibrary.Get(assetId), position, rotation, reader);
        }

        public static SmartNetIdentity Instantiate(Connection owner, NetworkId networkId, uint assetId, Vector3 position, Quaternion rotation, Reader reader = null)
        {
            return Instantiate(owner, networkId, IdentityLibrary.Get(assetId), position, rotation, reader);
        }

        public static SmartNetIdentity Instantiate(SmartNetIdentity prefab, Vector3 position, Quaternion rotation, Reader reader = null)
        {
            return Instantiate(null, SmartNetIdentity.GetNextNetworkId(), prefab, position, rotation, reader);
        }

        public static SmartNetIdentity Instantiate(Connection owner, SmartNetIdentity prefab, Vector3 position, Quaternion rotation, Reader reader = null)
        {
            return Instantiate(owner, SmartNetIdentity.GetNextNetworkId(), prefab, position, rotation, reader);
        }

        public static SmartNetIdentity Instantiate(Connection owner, NetworkId networkId, SmartNetIdentity prefab, Vector3 position, Quaternion rotation, Reader reader = null)
        {
            if (prefab == null)
            {
                return null;
            }

            return Spawn(Object.Instantiate(prefab, position, rotation), owner, networkId, reader);
        }

        public static SmartNetIdentity Spawn(SmartNetIdentity identity, Reader reader = null)
        {
            return Spawn(identity, null, SmartNetIdentity.GetNextNetworkId(), reader);
        }

        public static SmartNetIdentity Spawn(SmartNetIdentity identity, Connection owner, Reader reader = null)
        {
            return Spawn(identity, owner, SmartNetIdentity.GetNextNetworkId(), reader);
        }

        public static SmartNetIdentity Spawn(SmartNetIdentity identity, Connection owner, NetworkId networkId, Reader reader = null)
        {
            var spawningPlayer = identity.AssetId == (IdentityLibrary.Ins.Player?.AssetId ?? 0u);

            identity.SetOwnerInternal(owner);
            identity.SetNetworkId(networkId == NetworkId.Zero ? SmartNetIdentity.GetNextNetworkId() : networkId);
            identity.SetIsServer(NetworkServer.Active);
            identity.SetIsPlayer(spawningPlayer);

            if (spawningPlayer)
            {
                owner?.SetPlayer(identity);
                PrePlayerSpawnEvent(identity);
            }

            identity.OnPreStart();

            if (reader != null)
            {
                identity.OnDeserializeSpawnData(reader);
            }

            if (NetworkServer.Active)
            {
                identity.OnStartServer();
            }

            if (identity.IsOwner)
            {
                identity.OnStartOwner();
            }

            if (!NetworkServer.Active && !identity.IsOwner)
            {
                identity.OnStartClient();
            }

            Add(identity);

            if (spawningPlayer)
            {
                PlayerSpawnEvent(identity);
            }

            return identity;
        }

        public static void Destroy(GameObject gameObject)
        {
            Destroy(gameObject.GetComponent<SmartNetIdentity>());
        }

        public static void Destroy(SmartNetIdentity identity)
        {
            Unspawn(identity);

            Object.Destroy(identity.gameObject);
        }

        public static void Unspawn(GameObject gameObject)
        {
            Unspawn(gameObject.GetComponent<SmartNetIdentity>());
        }

        public static void Unspawn(SmartNetIdentity identity)
        {
            if (Remove(identity))
            {
                identity.OnPreStop();

                if (identity.IsServer)
                {
                    identity.OnStopServer();
                }

                if (identity.IsOwner)
                {
                    identity.OnStopOwner();
                }

                if (!identity.IsServer && !identity.IsOwner)
                {
                    identity.OnStopClient();
                }
            }
        }
    }
}
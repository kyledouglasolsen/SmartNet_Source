﻿using System;
using MonoBehaviour = UnityEngine.MonoBehaviour;

namespace SmartNet
{
    public class DefaultNetVisibility : MonoBehaviour, INetVisibility
    {
        private SmartNetIdentity myIdentity;
        private Action<SmartNetIdentity> onAddKnownIdentity, onRemoveKnownIdentity;

        public void OnStartServer()
        {
            myIdentity = GetComponent<SmartNetIdentity>();

            onAddKnownIdentity = onAddKnownIdentity ?? OnAddKnownIdentity;
            onRemoveKnownIdentity = onRemoveKnownIdentity ?? OnRemoveKnownIdentity;

            NetworkScene.KnownIdentities.AddEvent -= onAddKnownIdentity;
            NetworkScene.KnownIdentities.AddEvent += onAddKnownIdentity;

            NetworkScene.KnownIdentities.RemoveEvent -= onRemoveKnownIdentity;
            NetworkScene.KnownIdentities.RemoveEvent += onRemoveKnownIdentity;

            foreach (var identity in NetworkScene.KnownIdentities)
            {
                myIdentity.AddVisible(identity);
            }
        }

        public void OnStopServer()
        {
            NetworkScene.KnownIdentities.AddEvent -= onAddKnownIdentity;
            NetworkScene.KnownIdentities.RemoveEvent -= onRemoveKnownIdentity;
        }

        private void OnAddKnownIdentity(SmartNetIdentity identity)
        {
            myIdentity.AddVisible(identity);
        }

        private void OnRemoveKnownIdentity(SmartNetIdentity identity)
        {
            myIdentity.RemoveVisible(identity);
        }
    }
}
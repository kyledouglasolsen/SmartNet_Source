﻿using System.Collections.Generic;
using SmartNet.Messages;

namespace SmartNet
{
    public class MessageHandlers
    {
        private readonly Dictionary<uint, NetworkMessageDelegate> handlers = new Dictionary<uint, NetworkMessageDelegate>();

        public void Register<T>(NetworkMessageDelegate handler) where T : INetMessage
        {
            Register(MessageTypes.GetTypeId<T>(), handler);
        }

        public void Register(uint messageType, NetworkMessageDelegate handler)
        {
            handlers[messageType] = handler;
        }

        public void Unregister<T>() where T : INetMessage
        {
            Unregister(MessageTypes.GetTypeId<T>());
        }

        public void Unregister(uint messageType)
        {
            handlers.Remove(messageType);
        }

        public bool HasHandler<T>() where T : INetMessage
        {
            return handlers.ContainsKey(MessageTypes.GetTypeId<T>());
        }

        public bool HasHandler(uint messageType)
        {
            return handlers.ContainsKey(messageType);
        }

        public NetworkMessageDelegate Get<T>() where T : INetMessage
        {
            return handlers.TryGetValue(MessageTypes.GetTypeId<T>(), out var handler) ? handler : null;
        }

        public NetworkMessageDelegate Get(uint messageType)
        {
            return handlers.TryGetValue(messageType, out var handler) ? handler : null;
        }

        public void Clear()
        {
            handlers.Clear();
        }
    }
}
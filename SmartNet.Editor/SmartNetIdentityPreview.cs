﻿using CustomPreview = UnityEditor.CustomPreviewAttribute;
using ObjectPreview = UnityEditor.ObjectPreview;
using EditorStyles = UnityEditor.EditorStyles;
using GameObject = UnityEngine.GameObject;
using GUIContent = UnityEngine.GUIContent;
using GUIStyle = UnityEngine.GUIStyle;
using Color = UnityEngine.Color;
using Object = UnityEngine.Object;
using Rect = UnityEngine.Rect;
using Event = UnityEngine.Event;
using EventType = UnityEngine.EventType;
using Vector2 = UnityEngine.Vector2;
using RectOffset = UnityEngine.RectOffset;
using GUI = UnityEngine.GUI;
using System.Collections.Generic;

namespace SmartNet.Editor
{
    [CustomPreview(typeof(GameObject))]
    internal class SmartNetIdentityPreview : ObjectPreview
    {
        private List<SmartNetIdentityInfo> allInfo;
        private SmartNetIdentity identity;
        private GUIContent title;
        private Styles styles = new Styles();

        public override void Initialize(Object[] targets)
        {
            base.Initialize(targets);
            GetNetworkInformation(target as GameObject);
        }

        public override GUIContent GetPreviewTitle()
        {
            return title ?? (title = new GUIContent("Smart Net Identity Data"));
        }

        public override bool HasPreviewGUI()
        {
            return allInfo != null && allInfo.Count > 0;
        }

        public override void OnPreviewGUI(Rect r, GUIStyle background)
        {
            if (Event.current.type != EventType.Repaint)
            {
                return;
            }

            if (allInfo == null || allInfo.Count == 0)
            {
                return;
            }

            if (styles == null)
            {
                styles = new Styles();
            }

            var maxNameLabelSize = new Vector2(140f, 16f);
            var maxValueLabelSize = GetMaxNameLabelSize();
            var previewPadding = new RectOffset(-5, -5, -5, -5);
            r = previewPadding.Add(r);
            var initialX = r.x + 10f;
            var initialY = r.y + 10f;
            var labelRect = new Rect(initialX, initialY, maxNameLabelSize.x, maxNameLabelSize.y);
            var idLabelRect = new Rect(maxNameLabelSize.x, initialY, maxValueLabelSize.x, maxValueLabelSize.y);

            foreach (var info in allInfo)
            {
                GUI.Label(labelRect, info.Name, styles.LabelStyle);
                GUI.Label(idLabelRect, info.Value, styles.ComponentName);
                labelRect.y += labelRect.height;
                labelRect.x = initialX;
                idLabelRect.y += idLabelRect.height;
            }
        }

        private Vector2 GetMaxNameLabelSize()
        {
            var maxLabelSize = Vector2.zero;

            foreach (var info in allInfo)
            {
                var labelSize = styles.LabelStyle.CalcSize(info.Value);

                if (maxLabelSize.x < labelSize.x)
                {
                    maxLabelSize.x = labelSize.x;
                }

                if (maxLabelSize.y < labelSize.y)
                {
                    maxLabelSize.y = labelSize.y;
                }
            }

            return maxLabelSize;
        }

        private void GetNetworkInformation(GameObject gameObject)
        {
            identity = gameObject.GetComponent<SmartNetIdentity>();

            if (identity != null)
            {
                allInfo = new List<SmartNetIdentityInfo>
                {
                    GetAssetId(),
                    GetString("Network Id", identity.NetworkId.ToString()),
                    GetOwner(),
                    GetString("Is Server", identity.IsServer ? "True" : "False"),
                    GetString("Is Owner", identity.IsOwner ? "True" : "False"),
                    GetString("Is Player", identity.IsPlayer ? "True" : "False")
                };
            }
        }

        private SmartNetIdentityInfo GetAssetId()
        {
            var assetId = identity.AssetId.ToString();

            if (string.IsNullOrEmpty(assetId))
            {
                assetId = "<none>";
            }

            return GetString("Asset ID", assetId);
        }

        private SmartNetIdentityInfo GetOwner()
        {
            var owner = identity.Owner;

            if (owner == null)
            {
                return GetString("Owner", "Server");
            }

            return GetString("Owner", owner.ConnectionId.ToString());
        }

        private static SmartNetIdentityInfo GetString(string name, string value)
        {
            return new SmartNetIdentityInfo
            {
                Name = new GUIContent(name),
                Value = new GUIContent(value)
            };
        }

        private class SmartNetIdentityInfo
        {
            public GUIContent Name;
            public GUIContent Value;
        }

        private class Styles
        {
            public GUIStyle LabelStyle = new GUIStyle(EditorStyles.label);
            public GUIStyle ComponentName = new GUIStyle(EditorStyles.boldLabel);

            public Styles()
            {
                var fontColor = new Color(0.7f, 0.7f, 0.7f);

                LabelStyle.padding.right += 20;
                LabelStyle.normal.textColor = fontColor;
                LabelStyle.active.textColor = fontColor;
                LabelStyle.focused.textColor = fontColor;
                LabelStyle.hover.textColor = fontColor;
                LabelStyle.onNormal.textColor = fontColor;
                LabelStyle.onActive.textColor = fontColor;
                LabelStyle.onFocused.textColor = fontColor;
                LabelStyle.onHover.textColor = fontColor;

                ComponentName.normal.textColor = fontColor;
                ComponentName.active.textColor = fontColor;
                ComponentName.focused.textColor = fontColor;
                ComponentName.hover.textColor = fontColor;
                ComponentName.onNormal.textColor = fontColor;
                ComponentName.onActive.textColor = fontColor;
                ComponentName.onFocused.textColor = fontColor;
                ComponentName.onHover.textColor = fontColor;
            }
        }
    }
}